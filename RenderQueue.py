#! python
#

import os
import subprocess
import Tkinter as tk
import tkFileDialog
import multiprocessing
import psutil


class renderJob(object):
    """docstring for renderJob"""
    path_output = ""
    comp = ""
    isNetworkJob = False
    frame_start = ""
    frame_end = ""

    def __init__(self, projectPath, instanceAmount=1, outputFormat="png"):
        super(renderJob, self).__init__()
        self.path_project = projectPath
        self.output_format = outputFormat
        self.instances = instanceAmount

    def setComp(self, compositionName):
        self.comp = compositionName

    def setOutputPath(self, path):
        self.path_output = path

    def setFrameStart(self, index):
        self.frame_start = index

    def setFrameEnd(self, index):
        self.frame_end = index


class RenderQueueProgram(object):
    """docstring for RenderQueueProgram"""

    #aerender_path = r"C:\Program Files\Adobe\Adobe After Effects CS6\Support Files\
     #               aerender.exe"
    aerender_path = r"/home/tux/Documents/RenderQueue/parameter.py"

    def __init__(self):
        super(RenderQueueProgram, self).__init__()
        self.renderList = []

    def addJob(self, projectPath, instances, compositionName="", outputPath=""):
        newJob = renderJob(projectPath, instances)
        if (compositionName != ""):
            newJob.setComp(compositionName)
            if (outputPath != ""):
                newJob.setOutputPath(outputPath)
        self.renderList.append(newJob)

    def removeJob(self, index, a=None):
        self.renderList.pop(index)

    def renderAll(self, a=None):

        # TODO: the for loop below should only continue when its subprocesses are done rendering. (done?)
        #       render specific comp only with start/end and output path (error: no comp with this name/index ?!)

        print(' \n________________ \n| RENDERING LIST |\n ________________n')
        for i, job in enumerate(self.renderList):
            # run aerender.exe with corresponding parameters
            args = [self.aerender_path, "-project", job.path_project]
            processes = list()
            i = i + 1
            print "______  JOB %i ..." % i
            print "    Project: %s" % job.path_project

            # Strings are not passed properly as arguments ("no render settings template with given name")

            if (job.instances > 1 or job.isNetworkJob):
                args.extend(['-RStemplate', '"Multi-Machine Settings"', '-OMtemplate', '"Multi-Machine Sequence"'])

            if (job.comp != ""):
                args.extend(["-comp", job.comp])
                print "    Composition: %s" % job.comp
                if (job.frame_start != "" and job.frame_end != ""):
                    args.extend(["-s", job.frame_start, "-e", job.frame_end])
                    print "    Start Frame: %i" % job.frame_start
                    print "    End Frame: %i" % job.frame_end

                if (job.path_output != ""):
                    args.extend(["-output", job.path_output + r"\frames[####]." + job.output_format])
                    print r"    Output: %s\frames[####].%s" % (job.path_output, job.output_format)
            
            for j in xrange(1, int(job.instances) + 1):
                print "    Starting Process %i" % j
                processes.append(subprocess.Popen(args))

            processes[len(processes)-1].communicate()
            print "\n\n______  JOB %i ... DONE \n\n" % i


class AddProjectDialog(tk.Toplevel):
    def __init__(self, parent):
        tk.Toplevel.__init__(self)
        self.title("Add Project to Queue")
        self.minsize(width=300, height=250)
        self.parent = parent
        self.ok_button = tk.Button(self, text="testButton",command=self.on_ok)
        self.ok_button.grid(column=0,row=0)
      #  self.mainloop()

    def on_ok(self):
        # send the data to the parent
        #self.parent.new_data("tesst")
        file_path = tkFileDialog.askopenfilename(title='Select an After Effects Project File (.aep)', filetypes=(("After Effects Project Files", "*.aep"),  ))
        filename = os.path.basename(file_path)
        self.listbox.insert(tk.END,filename)

class RenderGui(object):
    """The GUI class. Uses a tk object."""
    master =  tk.Tk()
    master.title('Queued Rendering AE')
    master.minsize(width=660, height=500)
    master.columnconfigure(0, weight=1)
    master.rowconfigure(0, weight=1)

    frame_main = tk.Frame(master, padx=15, pady=15, bd="1", relief=tk.RAISED, bg="gray10")
    frame_main.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.E, tk.W))
    frame_main.columnconfigure(0, weight=1)
    frame_main.rowconfigure(0, weight=1)
    frame_main.rowconfigure(1, weight=1)

    frame_aepath = tk.Frame(frame_main, padx=10, pady=10, bd="1", bg="gray65")
    frame_aepath.grid(column=0,row=0,sticky=(tk.N,tk.E))
    label_aepath = tk.Label(frame_aepath,text="Path to After Effects Folder")
    label_aepath.grid(column=0,row=0,sticky=(tk.W))
    entry_aepath = tk.Entry(frame_aepath)
    entry_aepath.grid(column=0,row=1,sticky=tk.W)

    frame_list = tk.Frame(frame_main, padx=20, pady=20, bd="1", relief=tk.RAISED, bg="gray65")
    frame_list.grid(column=0, row=1, sticky=(tk.N, tk.S, tk.E, tk.W))
    frame_list.columnconfigure(0, weight=1)

    frame_controls = tk.Frame(frame_list, padx=10, pady=10, bd="1", relief=tk.RAISED, bg="gray60")
    frame_controls.grid(column=0, row=2, sticky=(tk.S, tk.E))

    scrollbar = tk.Scrollbar(frame_list)
    scrollbar.grid(column=1, row=1, sticky=(tk.N, tk.S, tk.E))

    listbox = tk.Listbox(frame_list, yscrollcommand=scrollbar.set,selectmode=tk.SINGLE)
    listbox.grid(column=0, row=1, sticky=(tk.N, tk.S, tk.E, tk.W))
    listbox.columnconfigure(0, weight=1)
    listbox.rowconfigure(0, weight=4)

    image_fileSelect = tk.PhotoImage(data='''\
            R0lGODlhIgAiAOf7AAAAAAAAMwAAZgAAmQAAzAAA/wArAAArMwArZgArmQArzAAr/wBVAABVMwBV
            ZgBVmQBVzABV/wCAAACAMwCAZgCAmQCAzACA/wCqAACqMwCqZgCqmQCqzACq/wDVAADVMwDVZgDV
            mQDVzADV/wD/AAD/MwD/ZgD/mQD/zAD//zMAADMAMzMAZjMAmTMAzDMA/zMrADMrMzMrZjMrmTMr
            zDMr/zNVADNVMzNVZjNVmTNVzDNV/zOAADOAMzOAZjOAmTOAzDOA/zOqADOqMzOqZjOqmTOqzDOq
            /zPVADPVMzPVZjPVmTPVzDPV/zP/ADP/MzP/ZjP/mTP/zDP//2YAAGYAM2YAZmYAmWYAzGYA/2Yr
            AGYrM2YrZmYrmWYrzGYr/2ZVAGZVM2ZVZmZVmWZVzGZV/2aAAGaAM2aAZmaAmWaAzGaA/2aqAGaq
            M2aqZmaqmWaqzGaq/2bVAGbVM2bVZmbVmWbVzGbV/2b/AGb/M2b/Zmb/mWb/zGb//5kAAJkAM5kA
            ZpkAmZkAzJkA/5krAJkrM5krZpkrmZkrzJkr/5lVAJlVM5lVZplVmZlVzJlV/5mAAJmAM5mAZpmA
            mZmAzJmA/5mqAJmqM5mqZpmqmZmqzJmq/5nVAJnVM5nVZpnVmZnVzJnV/5n/AJn/M5n/Zpn/mZn/
            zJn//8wAAMwAM8wAZswAmcwAzMwA/8wrAMwrM8wrZswrmcwrzMwr/8xVAMxVM8xVZsxVmcxVzMxV
            /8yAAMyAM8yAZsyAmcyAzMyA/8yqAMyqM8yqZsyqmcyqzMyq/8zVAMzVM8zVZszVmczVzMzV/8z/
            AMz/M8z/Zsz/mcz/zMz///8AAP8AM/8AZv8Amf8AzP8A//8rAP8rM/8rZv8rmf8rzP8r//9VAP9V
            M/9VZv9Vmf9VzP9V//+AAP+AM/+AZv+Amf+AzP+A//+qAP+qM/+qZv+qmf+qzP+q///VAP/VM//V
            Zv/Vmf/VzP/V////AP//M///Zv//mf//zP///////////////////yH+OUNSRUFUT1I6IGdkLWpw
            ZWcgdjEuMCAodXNpbmcgSUpHIEpQRUcgdjYyKSwgcXVhbGl0eSA9IDkwCgAh+QQBCgD/ACwAAAAA
            IgAiAAAI/gD/CRxIsKDBgwgTKlzIsKHDhxANKrsBYIUBixYBiIloMAaAjyBDKlNGjKTJkihHHswU
            suVHjwYAxJwpsyaASQbFuNzJs+WNnGJuBB0qtOhQMUiTKtWYc5/Tp1CjSoUq1OCNqViz7ovxs6AY
            rWCpbvQatuyNGAMziclkYBIat3DfynUrF41du/Wcxhj7r6ffnZOccun6z+PfwwCubiXMEvFhp1UF
            EnP8ePFAZYYp88y0lW9hzT2/RhaIBnRPywNLm965b/S/yZqFEtUJYJ8YtJdjUp76US1hgZkR8waA
            BmnB4Iffwp3UeO/vf5NWu+TqWaD0ndUFKhNYD9q/7t+9FIOv9w8a+X/bw3Ncz769+/fw4wcEADs=
            ''')

    def __init__(self):
        # self.protocol("WM_DELETE_WINDOW", self.quitProgram)
        # self.call('wm', 'iconphoto', self._w, self.image_programIcon)
        mem = psutil.virtual_memory()
        print "mem: %f MB" %(mem.total/(1024*1024*1024))
        print "cpus: %i" %multiprocessing.cpu_count()
        self.renderQ = RenderQueueProgram()
        #self.renderQ.addJob("test", "test")

        self.initButtons()
        self.master.mainloop()

    def on_show_dialog(self):
        dialog = AddProjectDialog(self.master)
       # dialog.show()

    def new_data(self, data):
        #self.listbox.insert(tk.END,data)
        pass

    def initButtons(self):
        button_chooseAEPath = tk.Button(self.frame_aepath, image=self.image_fileSelect, command=self.handle_setAEPath)
        button_chooseAEPath.grid(row=1,column=1,padx=10)
        button_render = tk.Button(self.frame_controls, text="RENDER ALL", command=self.renderQ.renderAll)
        button_render.grid(column=4, row=0, sticky=(tk.E, tk.W))
        button_addJob = tk.Button(self.frame_controls, text="add", command=self.handle_addJob)
        button_addJob.grid(column=2, row=0, sticky=(tk.E, tk.W))
        button_removeJob = tk.Button(self.frame_controls, text="remove", command=self.handle_removeJob)
        button_removeJob.grid(column=3, row=0, sticky=(tk.E, tk.W))

    def handle_renderAll():
        pass

    def handle_setAEPath(self, a=None):
        file_path = tkFileDialog.askdirectory(title='Select a Folder')
        self.entry_aepath.configure(text=file_path)
        print self.entry_aepath.get()
        pass

    def selectAEP(self):
        self.projectPath.set( tkFileDialog.askopenfilename(title='Select a .aep File', filetypes=(("After Effects Project Files", "*.aep"),  )) )
        self.frame_overrides.configure('enabled', True)
        print self.projectPath.get()       

    def handle_addJob(self, a=None):

        # TODO: Move into seperate class to ease loading an "edit settings" window

        jobProcesses = tk.IntVar()

        self.projectPath = tk.StringVar()

        Up = tk.Toplevel()
        Up.transient(self.master)
        Up.grab_set()
        Up.title("Add Project to render Queue")
        #Up.grid(row=0,column=0,pady=20, padx=20)
        pathFrame = tk.Frame(Up, bd="1", relief=tk.RAISED,pady=10, padx=10)
        pathFrame.grid(row=0,column=0,sticky=(tk.E,tk.W),columnspan=2,pady=20,padx=20)
        pathFrame.columnconfigure(0,weight=8)
        pathFrame.columnconfigure(1,weight=1)
        
        tk.Label(pathFrame, text ="Project Path:", font=('Times', 10),padx=20).grid(row=0,column=0, sticky="sw")

        entry_projectPath = tk.Entry(pathFrame, font=('Arial', 12,), textvariable=self.projectPath).grid(row=1,column=0,padx=20,sticky="ew")

        tk.Button(pathFrame, image = self.image_fileSelect,command=self.selectAEP).grid(row=1,column=1,padx=10, sticky=tk.E)
        tk.Label(Up, text ="Number of Processes: ", font=('Times', 10),padx=20,pady=10).grid(row=2,column=0,padx=50,sticky="e")
        spinbox_jobProcAmount = tk.Spinbox(Up, width=5,from_=1,to=multiprocessing.cpu_count(),textvariable=jobProcesses).grid(row=2,column=0,padx=10, sticky="e")
        tk.Checkbutton(Up, text="Override Project Setings").grid(row=3,column=0,sticky=tk.W,padx=20)
        self.frame_overrides = tk.Frame(Up, pady=5, padx=10, bd="1", relief=tk.RAISED )
        self.frame_overrides.grid(row=4, sticky="news", padx=20, pady=10,columnspan=2)
        tk.Label(self.frame_overrides, text="Compilation").grid(row = 0, sticky="w", padx = 20)
        tk.Entry(self.frame_overrides).grid(row=1, sticky="w", padx = 20)

       # tk.Label(self.frame_overrides, text="Compilation").grid(row = 0, sticky="w", padx = 20)
        for child in self.frame_overrides.winfo_children():
            child.configure(state='disable')

        Up.columnconfigure(0, weight=6)
        Up.columnconfigure(1,weight=1)

        Up.minsize(width=600, height=250)
        Button = tk.Button(Up, text ="Add to Queue", command = Up.destroy).grid(row =7,column=0,columnspan=2,pady=20,padx=20,sticky="se")

        self.master.wait_window(Up)
        path = self.projectPath.get()
        filename = os.path.basename(path)
        print "Adding Project %s ..." %path
        self.listbox.insert(tk.END, filename)
        self.renderQ.addJob(path, jobProcesses.get())
        print "Adding Project %s ... done." %path
 
    def handle_removeJob(self):
        index = int(self.listbox.curselection()[0])
        print "removing: %i" %index
        self.renderQ.removeJob(index)
        self.listbox.delete(index)


if __name__ == '__main__':
    #program = RenderQueueProgram()
    ##program.addJob(r"/home/tux/Documents/RenderQueue/test.aep", 2)
    #program.renderAll()
    renderer = RenderGui()
    #renderer.mainloop()
