Unlike the Render Queue in After Effects, this tool allows
queueing not only of Compositions, but entire Projects (and their render lists)
can be rendered automatically after each other.

The tool can report to the user about success/errors via eMail.