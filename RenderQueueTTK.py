#! python
# coding: latin-1

import os, sys
import subprocess
import ttk
import Tkinter as tk
import tkFileDialog
import tkMessageBox
import multiprocessing
import tooltip
import smtplib

reload(sys)
sys.setdefaultencoding('utf8')
#import psutil

default_ae_path = r"C:\Program Files\Adobe\Adobe After Effects CC"

class AutoScrollbar(tk.Scrollbar):
    # a scrollbar that hides itself if it's not needed.  only
    # works if you use the grid geometry manager.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        tk.Scrollbar.set(self, lo, hi)
    def pack(self, **kw):
        raise TclError, "cannot use pack with this widget"
    def place(self, **kw):
        raise TclError, "cannot use place with this widget"


class renderJob(object):
    """docstring for renderJob"""
    path_output = ""
    comp = ""
    isNetworkJob = False
    frame_start = ""
    frame_end = ""

    def __init__(self, projectPath, instanceAmount, outputFormat):
        super(renderJob, self).__init__()
        self.path_project = projectPath
        self.output_format = outputFormat
        self.instances = instanceAmount

    def setComp(self, compositionName):
        self.comp = compositionName

    def setOutputPath(self, path):
        self.path_output = path

    def setFrameStart(self, index):
        self.frame_start = index

    def setFrameEnd(self, index):
        self.frame_end = index


class RenderQueueProgram(object):
    """docstring for RenderQueueProgram"""

    aerender_path = default_ae_path
     #               aerender.exe"

    # TODO: automate change of after effects path in gui
    # aerender_path = r"/home/tux/Documents/projects/RenderQueue/parameter.py"

    email_user = 'improma.renderfarm@gmail.com'
    email_pwd = 'impromafarm'
    email_to = ""
    email_msg = "message"

    dict_cmd_eng = { "rstemp_multiMachine" : "Multi-Machine Settings",
                    "omtemp_multiMachine" : "Multi-Machine Sequence",
                    }
    dict_cmd_ger = { "rstemp_multiMachine" : "Sequenz für mehrere Rechner",
                    "omtemp_multiMachine" : "Einstellungen für mehrere Rechner",
                    }
    dict_cmd = { "English" : dict_cmd_eng,
                "German" : dict_cmd_ger,
                }

    ae_lang = ""

    def __init__(self):
        super(RenderQueueProgram, self).__init__()
        self.renderList = []

    def sendeMail(self):
        email_smtpserver = smtplib.SMTP("smtp.gmail.com",587)
        email_smtpserver.ehlo()
        email_smtpserver.starttls()
        email_smtpserver.ehlo
        email_smtpserver.login(email_user, email_pwd)
        header = 'To:' + self.email_to + '\n' + 'From:' + self.email_user + '\n' + 'Subject: Renderfarm Results\n'
        print header
        email_smtpserver.sendmail(email_user, email_to, self.email_msg)
        print 'done!'
        smtpserver.close()

    def addJob(self, projectPath, instances, picFormat, compositionName="", start = "", end = "", outputPath=""):
        newJob = renderJob(projectPath, instances, picFormat)
        if (compositionName != ""):
            newJob.setComp(compositionName)
            if (outputPath != ""):
                newJob.setOutputPath(outputPath)
            if start != "" or end != "":
                newJob.setFrameStart(start)
                newJob.setFrameEnd(end)
        self.renderList.append(newJob)

    def removeJob(self, index, a=None):
        self.renderList.pop(index)

    def renderAll(self, a=None):

        # TODO: the for loop below should only continue when its subprocesses are done rendering. (done?)
        #       render specific comp only with start/end and output path (error: no comp with this name/index ?!)

        print('\n ________________ \n| RENDERING LIST |\n|________________|\n')
        print 'using: '+self.aerender_path + r"/Support Files/aerender.exe"
        for i, job in enumerate(self.renderList):
            # run aerender.exe with corresponding parameters
            args = [self.aerender_path + r"/Support Files/aerender.exe", "-project", job.path_project]
            #args = [ "parameter" , "-project", job.path_project]
            processes = list()
            i = i + 1
            print "______  JOB %i ..." % i
            print "    Project: %s" % job.path_project

            # Strings are not passed properly as arguments ("no render settings template with given name")

            if (job.instances > 1 or job.isNetworkJob):
                args.extend(['-RStemplate', self.dict_cmd[ self.ae_lang ][ "rstemp_multiMachine" ], '-OMtemplate', self.dict_cmd[ self.ae_lang ][ "omtemp_multiMachine" ] ])

            if (job.comp != ""):
                args.extend(["-comp", job.comp])
                print "    Composition: %s" % job.comp
                if (job.frame_start != "" and job.frame_end != ""):
                    args.extend(["-s", job.frame_start, "-e", job.frame_end])
                    print "    Start Frame: %i" % job.frame_start
                    print "    End Frame: %i" % job.frame_end

                if (job.path_output != ""):
                    args.extend(["-output", job.path_output + r"\frames[####]." + job.output_format])
                    print r"    Output: %s\frames[####].%s" % (job.path_output, job.output_format)
            
            for j in xrange(1, int(job.instances) + 1):
                print "    Starting Process %i" % j
                processes.append(subprocess.Popen(args))

            processes[len(processes)-1].communicate()
            print "\n\n______  JOB %i ... DONE \n\n" % i
            if self.email_to != "":
                # TODO: Add information to message.
                self.email_msg = "Job " + str(i) + " of " + str(len(self.renderList)-1) + ":\n" + job.path_project + "\nis done rendering.\nPlease check for errors."
                self.sendeMail()

class jobSettings(tk.Toplevel): 
    def __init__(self,parent,editMode=False):       
        tk.Toplevel.__init__(self)
        self.title("Add Project to Queue")
        self.parent = parent
        self.transient(self.parent.master)
        self.columnconfigure(0, weight=1)

        self.minsize(width=650, height=250)
        
        self.compVar = tk.IntVar()
        self.rangeVar = tk.IntVar()
        self.formatVar = tk.StringVar()
        if not editMode:
            self.parent.buf_format.set('.psd')
        #self.columnconfigure(1,weight=1)

        frame_path = tk.Frame(self, bd="1", relief=tk.RAISED,pady=10, padx=10)
        frame_path.grid(row=0,pady=20,padx=40, sticky="ew")
        frame_path.columnconfigure(0,weight=10)
        frame_path.columnconfigure(1,weight=1)
        
        tk.Label(frame_path, text ="Project Path:", font=('Times', 10),padx=20).grid(row=0,column=0, sticky="sw")

        self.entry_path_project = tk.Entry(frame_path, justify="left",font=('Arial', 10,), textvariable=self.parent.buf_path_project)
        self.entry_path_project.grid(row=1,column=0,padx=20,sticky="ew")

        tk.Button(frame_path, image = self.parent.image_fileSelect,command=self.handle_setPath_project).grid(row=1,column=1,padx=10, sticky=tk.E)
        tk.Label(frame_path, text ="CPU Cores to use: ", font=('Times', 10),padx=20,pady=10).grid(row=2,column=0,sticky="w")
        tk.Label(frame_path, text ="Output Format: ", font=('Times', 10),padx=20,pady=10).grid(row=3,column=0,sticky="w")
        self.spinbox_instances = tk.Spinbox(frame_path, width=6,from_=1,to=multiprocessing.cpu_count(),textvariable=self.parent.buf_instances)
        self.spinbox_instances.grid(row=2,column=0, sticky="e")
        #options = 
        self.formatMenu = tk.OptionMenu(frame_path, self.parent.buf_format, ".psd", ".tiff", ".tga" ,".png")
        self.formatMenu.grid(row = 3, column = 0, sticky = "e" )


        self.frame_comp = tk.Frame(self, pady=10, padx=10, bd="1", relief=tk.RAISED )
        self.frame_comp.grid(row=4, sticky="news", padx=40, pady=10)
        self.frame_comp.columnconfigure(0,weight=2)

        self.frame_comp.columnconfigure(1,weight=1)
        self.frame_times = tk.Frame(self.frame_comp, pady = 5, padx = 10, bd="1", relief =tk.RAISED)
        self.frame_times.grid(row=2,column=1, rowspan = 2,sticky="nes")
        self.label_comp = tk.Label(self.frame_comp, text="Composition:")
        self.label_comp.grid(row = 1, sticky="w", padx = 20)

        self.check_comp = tk.Checkbutton(self, text="Specific Composition:",variable=self.compVar,command=self.toggleComp)
        self.check_comp.grid(row=3,column=0,sticky=tk.W,padx=30)
        self.check_range = tk.Checkbutton(self.frame_comp, text="Set Frame Range:",variable=self.rangeVar,command=self.toggleRange)
        self.check_range.grid(row=0,column=1,sticky=tk.W,padx=20)
        #tk.Label(self.frame_comp, text="Name:").grid(row = 0, column=0,sticky="w", padx = 20)
        tk.Label(self.frame_times, text="First Frame:").grid(row = 0,column=1, sticky="w", padx = 20)
        tk.Label(self.frame_times, text="Last Frame:").grid(row = 2, column=1,sticky="w", padx = 20)
        self.entry_comp = tk.Entry(self.frame_comp,textvariable=self.parent.buf_comp)
        self.entry_comp.grid(row=2, sticky="w", padx = 20)
        self.entry_start = tk.Entry(self.frame_times,width=10, textvariable=self.parent.buf_startFrame).grid(row=1, column=1, sticky="w", padx = 20)
        self.entry_end = tk.Entry(self.frame_times, width=10,textvariable=self.parent.buf_endFrame).grid(row=3, column=1, sticky="w", padx = 20)
        
        tk.Label(self.frame_comp, text="Output Path (Optional):").grid(row = 4, sticky="w", padx = 20)
        self.entry_path_output = tk.Entry(self.frame_comp,textvariable=self.parent.buf_path_output)
        self.entry_path_output.grid(row=5, columnspan=2,padx = 20,sticky="ew")
        #self.entry_path_output.columnconfigure
        tk.Button(self.frame_comp, image = self.parent.image_fileSelect,command=self.handle_setPath_Output).grid(row=5,column=3,padx=10, sticky=tk.E)

        self.disableChildren(self.frame_comp)
        
        if not editMode:
            self.spinbox_instances.configure(state='disabled')
            self.formatMenu.configure(state='disabled')
            self.check_comp.configure(state='disabled')        
        else:
            print "comp val:" + self.parent.buf_comp.get()
            if self.parent.buf_comp.get() != "":
                self.compVar.set(1)
                self.toggleComp()
                if (self.parent.buf_startFrame.get().isdigit() or self.parent.buf_endFrame.get() != ""):
                    self.rangeVar.set(1)
                    self.toggleRange()
        
        buttonText = "Add to Queue"
        if editMode: buttonText = "Finsh Edit"

        Button = tk.Button(self, text = buttonText, command = self.destroy).grid(row =7,column=0,pady=20,padx=20,sticky="se")
        self.grab_set()

    def toggleComp(self):
               # tk.Label(self.frame_comp, text="Compilation").grid(row = 0, sticky="w", padx = 20)
        if self.compVar.get() == 0:
            self.disableChildren(self.frame_comp)
            self.parent.buf_comp.set("")
            self.parent.buf_startFrame.set("")
            self.parent.buf_endFrame.set("")
            self.parent.buf_path_output.set("")
        else:
            #self.check_range.configure(state='normal')
            #self.entry_comp.configure(state='normal')
            #self.label_comp.configure(state='normal')
            for child in self.frame_comp.winfo_children():
                if not isinstance(child,tk.Frame) :
                    child.configure(state='normal')

    def toggleRange(self):
               # tk.Label(self.frame_comp, text="Compilation").grid(row = 0, sticky="w", padx = 20)
        if self.rangeVar.get() == 0:
            self.disableChildren(self.frame_times)
            self.parent.buf_startFrame.set("")
            self.parent.buf_endFrame.set("")
        else:

            for child in self.frame_times.winfo_children():
                if not isinstance(child,tk.Frame) :
                   child.configure(state='normal')

    def handle_setPath_Output(self, a=None):
        file_path = tkFileDialog.askdirectory(title='Select a Folder')
        self.parent.buf_path_output.set(file_path)
        print self.entry_path_output.get()


    def handle_setPath_project(self):
        self.parent.buf_path_project.set( tkFileDialog.askopenfilename(title='Select a .aep File', filetypes=(("After Effects Project Files", "*.aep"),  )) )
        # if we have a project Path
        if self.parent.buf_path_project.get() != "":
            self.entry_path_project.focus_set()
            size = len(self.parent.buf_path_project.get())
            self.spinbox_instances.configure(state='normal')
            self.formatMenu.configure(state='normal')
            self.check_comp.configure(state='normal')

            self.entry_path_project.icursor(size)
            self.entry_path_project.icursor(size-1)
            self.entry_path_project.icursor(size)
        print self.parent.buf_path_project.get()       

    def disableChildren(self, frame):
        for child in frame.winfo_children():
            if isinstance(child,tk.Frame) :
                self.disableChildren(child)
            else:
                child.configure(state='disabled')

class RenderGui(object):
    """The GUI class. Uses a tk object."""
    master = tk.Tk()

    dict_eng = {"ae_path" : "Path", 
                "ae_lang" : "Language", 
                "ae_lang_prompt" : "Set AE Language", 
                "header_pro" : "Project",  
                "header_cores" : "Cores", 
                "header_format" : "Format", 
                "header_comp" : "Composition", 
                "header_first" : "First Frame",
                "header_last" : "Last Frame",
                "header_output" : "Output Path",    
                "tooltip_btn_AEchoose" : "Navigate to your\nAfter Effects\nInstallation", 
                "tooltip_btn_proAdd" : "Add a job to the render list.", 
                "tooltip_btn_proRem" : "Remove a job from the render list.",
                "tooltip_btn_proEdit" : "Edit a job in the render list.",  
                "tooltip_btn_render" : "Render all jobs in list.",
                "tooltip_entry_email" : "If you'd like to be notified via eMail\nspecify the address here.",
                "job_title_add" : "Add job",  
                "job_title_edit" : "Edit job",
                "job_project" : "Project:",  
                "job_cores" : "CPU Cores to use:", 
                "job_format" : "Output Format:",
                "job_check_comp" : "Specify Composition:",
                "job_check_range" : "Set Frame Range:",  
                "job_comp" : "Composition:",
                "job_frame_first" : "First Frame:",
                "job_frame_last" : "Last Frame:",
                "job_outputPath" : "Output Path:",
                "job_btn_add" : "Add Job:",
                "job_btn_edit" : "Edit Job:",
                }

    dict_ger = {"ae_path" : "Pfad", 
                "ae_lang" : "Sprache", 
                "ae_lang_prompt" : "AE Sprache wählen", 
                "header_pro" : "Projekt",  
                "header_cores" : "Kerne", 
                "header_format" : "Format", 
                "header_comp" : "Komposition", 
                "header_first" : "Erstes Bild",
                "header_last" : "Letztes Bild",
                "header_output" : "Output Pfad",    
                "tooltip_btn_AEchoose" : "Navigiere zum\nAfter Effects\nInstallationsordner", 
                "tooltip_btn_proAdd" : "Hinzufügen eines Auftrages\nan die Renderliste.", 
                "tooltip_btn_proRem" : "Entfernen eines Auftrages\naus der Renderliste.",
                "tooltip_btn_proEdit" : "Verändern eines Auftrages\nin der Renderliste.",  
                "tooltip_btn_render" : "Renderliste rendern",
                "tooltip_entry_email" : "Bei erwünschter eMail Benachrichtigung\ndie Adresse hier angeben.",
                "job_title_add" : "Auftrag hinzufügen",
                "job_title_edit" : "Auftrag bearbeiten",
                "job_project" : "Projekt:",  
                "job_cores" : "Zugeordnete CPU Kerne:", 
                "job_format" : "Ausgangs Format:",
                "job_check_comp" : "Komposition angeben:",
                "job_check_range" : "Bildspanne:",  
                "job_comp" : "Komposition:",
                "job_frame_first" : "Erstes Bild:",
                "job_frame_last" : "Letztes Bild:",
                "job_outputPath" : "Ausgangspfad:",
                "job_btn_add" : "Auftrag hinzufügen",
                "job_btn_edit" : "Auftrag bearbeiten",
                }

    dictionary_ui = { "English" : dict_eng,
                "German" : dict_ger,
                }

    #global image_fileSelect
    image_fileSelect = tk.PhotoImage(data='''\
        R0lGODlhIgAiAOf7AAAAAAAAMwAAZgAAmQAAzAAA/wArAAArMwArZgArmQArzAAr/wBVAABVMwBV
        ZgBVmQBVzABV/wCAAACAMwCAZgCAmQCAzACA/wCqAACqMwCqZgCqmQCqzACq/wDVAADVMwDVZgDV
        mQDVzADV/wD/AAD/MwD/ZgD/mQD/zAD//zMAADMAMzMAZjMAmTMAzDMA/zMrADMrMzMrZjMrmTMr
        zDMr/zNVADNVMzNVZjNVmTNVzDNV/zOAADOAMzOAZjOAmTOAzDOA/zOqADOqMzOqZjOqmTOqzDOq
        /zPVADPVMzPVZjPVmTPVzDPV/zP/ADP/MzP/ZjP/mTP/zDP//2YAAGYAM2YAZmYAmWYAzGYA/2Yr
        AGYrM2YrZmYrmWYrzGYr/2ZVAGZVM2ZVZmZVmWZVzGZV/2aAAGaAM2aAZmaAmWaAzGaA/2aqAGaq
        M2aqZmaqmWaqzGaq/2bVAGbVM2bVZmbVmWbVzGbV/2b/AGb/M2b/Zmb/mWb/zGb//5kAAJkAM5kA
        ZpkAmZkAzJkA/5krAJkrM5krZpkrmZkrzJkr/5lVAJlVM5lVZplVmZlVzJlV/5mAAJmAM5mAZpmA
        mZmAzJmA/5mqAJmqM5mqZpmqmZmqzJmq/5nVAJnVM5nVZpnVmZnVzJnV/5n/AJn/M5n/Zpn/mZn/
        zJn//8wAAMwAM8wAZswAmcwAzMwA/8wrAMwrM8wrZswrmcwrzMwr/8xVAMxVM8xVZsxVmcxVzMxV
        /8yAAMyAM8yAZsyAmcyAzMyA/8yqAMyqM8yqZsyqmcyqzMyq/8zVAMzVM8zVZszVmczVzMzV/8z/
        AMz/M8z/Zsz/mcz/zMz///8AAP8AM/8AZv8Amf8AzP8A//8rAP8rM/8rZv8rmf8rzP8r//9VAP9V
        M/9VZv9Vmf9VzP9V//+AAP+AM/+AZv+Amf+AzP+A//+qAP+qM/+qZv+qmf+qzP+q///VAP/VM//V
        Zv/Vmf/VzP/V////AP//M///Zv//mf//zP///////////////////yH+OUNSRUFUT1I6IGdkLWpw
        ZWcgdjEuMCAodXNpbmcgSUpHIEpQRUcgdjYyKSwgcXVhbGl0eSA9IDkwCgAh+QQBCgD/ACwAAAAA
        IgAiAAAI/gD/CRxIsKDBgwgTKlzIsKHDhxANKrsBYIUBixYBiIloMAaAjyBDKlNGjKTJkihHHswU
        suVHjwYAxJwpsyaASQbFuNzJs+WNnGJuBB0qtOhQMUiTKtWYc5/Tp1CjSoUq1OCNqViz7ovxs6AY
        rWCpbvQatuyNGAMziclkYBIat3DfynUrF41du/Wcxhj7r6ffnZOccun6z+PfwwCubiXMEvFhp1UF
        EnP8ePFAZYYp88y0lW9hzT2/RhaIBnRPywNLm965b/S/yZqFEtUJYJ8YtJdjUp76US1hgZkR8waA
        BmnB4Iffwp3UeO/vf5NWu+TqWaD0ndUFKhNYD9q/7t+9FIOv9w8a+X/bw3Ncz769+/fw4wcEADs=
        ''')

    image_minus=tk.PhotoImage(data='''\
        R0lGODlhIgAiAMZ3AAAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ8P
        DxERERISEhMTExQUFBUVFRoaGhwcHB0dHR4eHiAgICMjIyYmJikpKSoqKi0tLS8vLzAwMDIyMjQ0
        NDU1NTc3Nzo6Ojw8PD09PUJCQkNDQ0VFRUZGRkhISElJSUxMTE1NTU5OTlFRUVVVVVhYWFlZWVxc
        XF1dXWBgYGJiYmNjY2RkZGVlZWpqamtra2xsbG5ubnV1dXd3d3x8fH5+fn9/f4GBgYKCgoODg4SE
        hIaGhoeHh4iIiIqKio2NjZGRkZKSkpeXl5ubm6CgoKGhoaOjo6WlpaampqioqKmpqaqqqqurq7Gx
        sbKysrS0tLW1tbu7u8DAwMLCwsTExMjIyMrKysvLy8/Pz9TU1NXV1dbW1tfX19jY2Nra2tzc3OHh
        4eLi4uPj4+Tk5OXl5efn5+np6erq6uzs7PX19f///////////////////////////////////yH+
        EUNyZWF0ZWQgd2l0aCBHSU1QACH5BAEKAH8ALAAAAAAiACIAAAf+gH+Cg4SEcGFdXGFvhY2OjlpH
        Pz49NjAmFhUxVo+dg1JGSTMZCgEApwMCAAEXTJ6NYkNJH6antre2HGCvglNILgO4wrgKTq9PRRrD
        y7cDSJ1VQBTM1KcEUI5nNxvV3Q1ljTQt3eQlhVsvBeTdAVeEKx7r5CKDZheq8tUFZIJL6vndhghy
        AZAcCkEZcA2AEKGhw4cQGx7AZUHQAlwY3NDZyLGjx400cCUQFOxWBjt3UqpcyTJlDoV0/hjAdbKl
        zZUvbxWo82cCLgpcvAgdSrSoUBa4HggaUbBbB0E8mlajIUhLLanCAkQZNA2rsAhxBgHxKkwHITkS
        yNpykKZQk6tuWAUIcUTQqwAVj+bQkiqABJtOcUI0NZBCzas6Mkqui4CjDS9BVBJ2UzBCymNDRDIM
        gHuqwAMQRtZc/vOGTBYlOlJcQCAgQAACCBg8sHAihxIsYxjxkoPmi5UmQXbUiAEDRowaO4I0sfLl
        jBxHgQAAOw==
        '''
        )


    image_plus = tk.PhotoImage(data='''\
        R0lGODlhIgAiAOeBAAAAAAEBAQICAgMDAwQEBAUFBQYGBggICAkJCQoKCgsLCwwMDA0NDQ8PDxER
        ERISEhMTExQUFBUVFRoaGhwcHB0dHR4eHiMjIyQkJCYmJikpKSoqKi0tLS8vLzAwMDIyMjQ0NDU1
        NTc3Nzk5OTo6Ojw8PD09PUJCQkNDQ0VFRUZGRkhISExMTE1NTU5OTlFRUVVVVVhYWFlZWVxcXF1d
        XWBgYGFhYWJiYmNjY2RkZGVlZWpqamtra2xsbG1tbW5ubm9vb3V1dXd3d3x8fH5+fn9/f4CAgIGB
        gYKCgoODg4SEhIaGhoeHh4iIiIqKioyMjI2NjY6OjpGRkZKSkpSUlJeXl5ubm6CgoKGhoaOjo6Wl
        paampqioqKmpqaqqqqurq7GxsbKysrS0tLW1tbm5ubq6usDAwMLCwsTExMXFxcjIyMrKysvLy8/P
        z9TU1NXV1dbW1tfX19jY2Nra2tzc3OHh4ePj4+Tk5OXl5efn5+np6erq6uvr6+zs7Pr6+vz8/P7+
        /v//////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////yH5BAEKAP8ALAAAAAAiACIA
        AAj+AP8JHEiQ4B00Y8SgsVOwoUOHYJQE+dGjxgsTFSjA4PKw40AsSZjIuJAgAICTAwQACGABiseG
        aoow8WDypM2bNjeceSkwy5IWA3AKxZlgyssqSDIMXXpzwJKOW4RMGIrBxhAjPjgsJWDF4ZsbGpY+
        AUQWkBSmDNo0nMGCaRRAgeJOYQqgRMEwLgq4hSuXboAuBFd0oPs2bqC5dEMMdGNB5V7DiJkWYCPQ
        iV7CfA/TPVlEYIvNAAr33YxC4IWbD2jUWM2aTOY0NlizxnCzgkAFN1P4Kcs7c++yRG4iEBjU5ozM
        hpMrT06l6Z5/Bm7SQL68etyzNgv0+Sfh5oo/1sPTxxVy04FAETctlDnDvj2fzHjayx9xk4NAHqCn
        ZI7MdIZAMDU9NhpTAVwx0FSYQbYZBHgMRF6CAy6lA0F5RAChZkw1EEdBUgQolGgYDiUAEQ59tpRo
        gPB3kwAqPKQHTUNp9AMQMVggIgl0dIQHCKANZUAKcrzURwzF9QgABDjUwZNAWpwGWgIiYLGkQUdc
        MICHJxXgwAdJzDHlP3aw8UUTOqRgwQECBBAAAQcs4EAFJ+TQhBdrMMRTHnCYwYUUQ+xAAwwvvAAD
        DTsMIQUXZryRh0MBAQA7
        '''
        )

    image_config = tk.PhotoImage(data='''\
        R0lGODlhIgAiAOewAAAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4O
        Dg8PDxERERISEhMTExQUFBYWFhcXFxgYGBkZGRoaGhsbGx8fHyEhISIiIiMjIyQkJCUlJSYmJioq
        KisrKywsLC0tLS8vLzAwMDExMTIyMjMzMzQ0NDY2Njc3Nzg4ODo6Ojs7Ozw8PEBAQEJCQkdHR0pK
        SktLS01NTU5OTlBQUFFRUVJSUlNTU1VVVVZWVldXV1hYWFlZWVtbW1xcXF1dXV9fX2BgYGFhYWRk
        ZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG5ubm9vb3BwcHJycnNzc3V1dXh4eHl5eXp6en19fYCA
        gIGBgYKCgoODg4SEhIaGhoeHh4iIiImJiYqKiouLi42NjY+Pj5CQkJKSkpaWlpeXl5iYmJmZmZub
        m5ycnJ6enp+fn6KioqOjo6SkpKWlpaampqurq6ysrK2trbCwsLS0tLW1tba2tre3t7q6uru7u7y8
        vL29vb6+vr+/v8DAwMHBwcPDw8fHx8jIyMzMzM3Nzc7Ozs/Pz9DQ0NHR0dPT09XV1dfX19jY2NnZ
        2dra2tvb293d3d7e3t/f3+Dg4OHh4ePj4+Tk5Ofn5+jo6Onp6erq6uvr6+zs7O3t7e7u7vDw8PLy
        8vT09PX19fb29vj4+Pn5+fv7+/z8/P39/f7+/v//////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////yH+EUNyZWF0ZWQgd2l0aCBH
        SU1QACH5BAEKAP8ALAAAAAAiACIAAAj+AP8JHEiwICM+ePYoKsiwYcNKarJIiaIEyIwREVJokeSw
        o8BLZMRQcQFhAICTAEwCUABkkkeGc8AwgYCypk0ADcq8HNjli4ebQFEKELLTypUFQFn8OBEUgA2P
        WqgYAOpA0KpHCIIGOOLwThMHQWWQgtWKRlMBcRryGIEyRJkaSCm0afXqVaAMABzUcMOiJgZPBcfU
        QDkBUCtVjNIkSlX3lStOcAKVcnUJ7MkAWQq2uIDSgKFXsBrDGk26bug+AWpWIKjHgs0irEiPFi37
        FQ6bAe4MZCLAJoRNoEND8sJkzKXgsEpltWlkYIqbKDrVdXUmAUoHdYK7inGTxEAJNQX+dFjTKrQf
        AjYXNJpN6ERvlA9GCUR/kgUbS6tAv7oB1ElpU4PkgNIAmfzzyXsB1EFXaKF1AFQMsoVGCEoBUDLf
        ZW640lhdKwC1Q22swDEgJgKBd9ICOuShSmNfACUHaafA4YJKADwQikAq3FSFK6OZAoNNOqwy2xM3
        ibDbTRd8ol8pVmjAwAdfMDbaKRHcNMRAeryH0gaZIOcYKhqGBlorJuBGB0Ea2ASFkBHWpp+INVFw
        40Bg1NRAIfqZ5qVpyVVA4RQMbYASAWmw8sgSJZBRSnCjcCGCE4ywggZ9AFigCUN3aGmACweghAWP
        r2SBUgEl0EioQ1GkBtQGoLySygd9QRmAhEc7qIrbGqLYERQCPojyEhFaNmXTA0v4uhMaD9gq7AAg
        mLETQZQEYV1TBWCQxCLPDuSJJIWEYUICAagqAAINVPDCFoFE0km2lhQCxxU9tMCBBAwggMACDUiw
        QQs8WPFGIZZkK9AonFDiCCKFAPIHIIQc0gglnMjnUEAAOw==
        '''
        )

    image_goal = tk.PhotoImage(data='''\
        R0lGODlhMAAwAOfSAAAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4O
        Dg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxkZGRoaGhsbGxwcHB8fHyAgICEhISIiIiMjIyQk
        JCUlJScnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDIyMjMzMzQ0NDU1NTY2Njc3Nzk5OTo6
        Ojs7Ozw8PD09PT4+Pj8/P0JCQkNDQ0VFRUZGRkhISElJSUpKSk1NTU5OTk9PT1BQUFFRUVJSUlNT
        U1VVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2ho
        aGpqamtra2xsbG1tbW5ubnBwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3l5eXp6ent7e3x8fH19fX5+
        foCAgIGBgYKCgoSEhIWFhYeHh4iIiImJiYuLi42NjY6OjpCQkJGRkZKSkpOTk5SUlJWVlZeXl5iY
        mJmZmZqampycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKampqioqKqqqqurq6ysrK2tra6urrGx
        sbOzs7S0tLW1tba2tre3t7u7u7y8vL6+vr+/v8DAwMHBwcLCwsPDw8XFxcfHx8jIyMnJycrKysvL
        y87Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dfX19jY2NnZ2dra2tzc3N7e3t/f3+Dg4OHh4eLi4uPj
        4+Tk5OXl5ebm5ufn5+np6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PHx8fLy8vPz8/X19fb29vf39/n5
        +fr6+vv7+/z8/P39/f7+/v//////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////yH+EUNyZWF0ZWQgd2l0aCBH
        SU1QACH5BAEKAP8ALAAAAAAwADAAAAj+AP8JHEiw4MBUiMww8WEjBg4kZxStMkixosWBptow8TIn
        ECA7bchgEVJCAYMUZ1BdXEnRUhU1kPQkQRFBAICbNwskCHDzwI9MLFmmyjLnEZQJPHEqXXozQAEh
        roJW5OPFkRIGTLNqrdBHakEseOhY0Eo2K4EoXv/tQlJoic2ycJUG6OEVCSEcSePqBTA3qJI8M/YK
        bqpkJRoyPgYrDhDHYqYeW94CqCCisuXLmDNbBkFAqYNPFW+4QYAzwBpevVKrXs26depZGJbSoNhH
        yIqlGlpAiobqhQ5d0f600JJMGpwWaZhFc9LiELRhOlgUWDqgkEESUvIqVQPtU4AEpqL+qQGQIpg0
        KQB0IIvGAoCXZ74kaDVRMFGKD0xfRNmdi8sVWtF4QoUax0gziRR0LBNNH1EoAk0ybDShAFMBREIQ
        DylI1pQbwhBjjDPM5KLLLrrggssuJJqIYi7FPJOLK6yMckFWQgxkiwMTZOWABjxSsksGFAQp5JBC
        pvGMEgokqR1OFNgi0CIBLMkUH7gYoNcTzQwBVwCQCJSFXlRaGReWWsIVhkA0gFnllVnGtYNAHagp
        JlxkxhXCP7pUICebZZZlwT+zQLDnmG3CFcEtsjwwKJ2FlgUBLbXIF1eYfMY1QS7/xDbpmoT2SVYG
        ApWwaFl1wqWCQEGMSlapZR0hEBz+qmrFqlYByCHQJxpqRWmncBXAyUAgbDpnWVA0Y0SUUt40AkFd
        JItDGGKIAYoyhthh7bXYXmvJM6gUQogfEWT1BUGoDNvUFJXw8owwmGyinCyXnPJMNK9cosq8oVyS
        SzTMaCIJBUwZUEpBRdAqR3cCOKBKNGwEAMMw0ogRAA7GRNNCAGZA8wtSFLpa0CgNBLzAG9CI8gAF
        p0QzBwMyCCMNGwvgQEw0OSwwxjPAeKDAkg54QhEXSwXQhSi38JtKKcdEY0wpqjQjDTGkrNJMNLmM
        AiA0sWwyVmlbWHSCdkLoscfYZJdt9tlj2yFoUy04WREoiiomWAAdaLLSIaTJrZe8B4QEtUfeemsl
        gAl7eCVIyIEzhYALgqT1DyYcJK7UBDtM4rhAtigxXeAHlMBFVJcPJEkMnQ12wAdFPBI6RYbUYG5W
        AVDwARGE4LL6QLGMUokhdYABxAYHCBDlTQIUsAAEFWzggQ9f1HGIJaTIcrkurHDCiB1hLKHDCh1Q
        wMABBRAwAAEGGIAAAxR0wMIOTYhxRyOdtLJL6Lm8QkomjwxyhxtkcGFFFFCAQhSswIUyvAEPhICE
        JkoBC12sJCAAOw==
        '''
        )

    # buffers for passing values from one window(object) to another

    buf_ae_path = tk.StringVar()
    buf_ae_path.set( default_ae_path )
    buf_ae_lang = tk.StringVar()
    buf_ae_lang.set('Set AE Language')
    buf_path_project = tk.StringVar()
    buf_instances = tk.IntVar()
    buf_format = tk.StringVar()
    buf_comp = tk.StringVar()
    buf_startFrame = tk.StringVar()
    buf_endFrame = tk.StringVar()
    buf_path_output = tk.StringVar()
    buf_email = tk.StringVar()

    

    def __init__(self):
        self.renderQ = RenderQueueProgram()
        self.buf_ae_lang.trace("w", self.updateQ)
        self.buf_email.trace("w", self.updateMailAddress)
        self.initWidgets()
        self.tree.bind("<Double-1>", self.OnDoubleClick)
        print "cpus: %i" %multiprocessing.cpu_count()

        self.initButtons()
        self.master.mainloop()

    def initWidgets(self):
        self.master.title('Queued Rendering AE')
        self.master.minsize(width=780, height=550)
        self.master.columnconfigure(0, weight=1)
        self.master.rowconfigure(0, weight=1)

        self.frame_main = tk.Frame(self.master, padx=15, pady=15, bd="1", relief=tk.RAISED, bg="gray10")
        self.frame_main.grid(row=0, sticky=(tk.N, tk.S, tk.E, tk.W))
        self.frame_main.columnconfigure(0, weight=1)
        self.frame_main.rowconfigure(0, weight=1)
        self.frame_main.rowconfigure(1, weight=6)

        self.frame_aepath = tk.Frame(self.frame_main, padx=20, pady=10, bd="1", relief=tk.RAISED ,bg="gray65")
        self.frame_aepath.grid(column=0,pady=10,row=0,sticky="news")
        self.frame_aepath.columnconfigure(0,weight=1)
        self.frame_aepath.columnconfigure(1,weight=4)
        self.frame_aepath.columnconfigure(2,weight=1)
        self.label_aeinfo = tk.Label(self.frame_aepath,text="After Effects Information")
        self.label_aeinfo.grid(row=0,sticky=(tk.W), columnspan=2)
        self.label_aepath = tk.Label(self.frame_aepath,text="Path:")
        self.label_aepath.grid(row=1,column= 0,sticky=(tk.W))
        self.entry_aepath = tk.Entry(self.frame_aepath, textvariable= self.buf_ae_path)
        self.entry_aepath.grid(row=1,column= 1,sticky='ew')
        self.label_aelang = tk.Label(self.frame_aepath,text="Language:")
        self.label_aelang.grid(row=2,column= 0,sticky=(tk.W))

        self.list_lang = ('German', 'English')
        #menu_aeLang = apply(ttk.OptionMenu, (frame_aepath, buf_ae_lang) + tuple(list_lang))
        self.menu_aeLang = tk.OptionMenu(self.frame_aepath, self.buf_ae_lang, *self.list_lang)
        self.menu_aeLang.grid(row = 2, column = 1, sticky = "e" )

        self.frame_list = tk.Frame(self.frame_main, padx=20, pady=20, bd="1", relief=tk.RAISED, bg="gray65")
        self.frame_list.grid(column=0, row=1, sticky=(tk.N, tk.S, tk.E, tk.W))
        self.frame_list.columnconfigure(0, weight=1)

        self.frame_controls = tk.Frame(self.frame_list, padx=10, pady=10, bd="1", relief=tk.RAISED, bg="gray60")
        self.frame_controls.grid(column=0, row=2, sticky=(tk.S, tk.E), rowspan=2)


        self.frame_email = tk.Frame(self.frame_list, width=40)
        self.frame_email.grid(row=2,column=0, sticky=(tk.W))

        tooltip.createToolTip(self.frame_email,"If you'd like to be notified via eMail\nspecify the address here.")

        self.label_email = tk.Label(self.frame_email, text = "eMail Address: " )
        self.label_email.grid(row=2,column=0,sticky=(tk.W),columnspan=2)
        self.entry_email = tk.Entry(self.frame_email, textvariable=self.buf_email, width=40)
        self.entry_email.grid(row=3,column=0,sticky=(tk.W),columnspan=2)
        # "Enter eMail Address if youd like to be notified."

        self.scrollbar = AutoScrollbar(self.frame_list)
        self.scrollbar.grid(column=1, row=1, sticky=(tk.N, tk.S, tk.E))

        tk.Label(self.frame_list,text="Projects/Compositions to render:").grid(row=0,sticky="w")
        self.tree = ttk.Treeview(self.frame_list, yscrollcommand=self.scrollbar.set, selectmode = 'browse')
       # tree.bind("<Double-1>", OnDoubleClick)
        self.tree['columns'] = ('instances', 'format', 'comp', 'start','end','outputPath')
        self.tree.heading("#0", text="Project")
        self.tree.column("#0",minwidth=100,width=120, stretch=tk.YES)
        self.tree.heading('instances',text='Cores')
        self.tree.column("instances",minwidth=50,width=50, stretch=tk.YES)
        self.tree.heading('comp',text='Compilation')
        self.tree.column("comp",minwidth=80,width=100, stretch=tk.YES)
        self.tree.heading('start',text='First Frame')
        self.tree.column("start",minwidth=80,width=100, stretch=tk.YES)
        self.tree.heading('end',text='Last Frame')
        self.tree.column("end",minwidth=80,width=100, stretch=tk.YES)
        self.tree.heading('format',text='Format')
        self.tree.column("format",minwidth=60,width=60, stretch=tk.YES)
        self.tree.heading('outputPath',text='Output Path')
        self.tree.column("outputPath",minwidth=80,width=100, stretch=tk.YES)
        self.tree.grid(row=1, sticky = "ew")
        self.scrollbar.config(command=self.tree.yview)

        self.progressBar = ttk.Progressbar(self.frame_list, orient='horizontal', mode='indeterminate')

    def updateMailAddress(self, a=None, b= None, c=None):
        temp = self.buf_email.get()
        if '@' and '.' in temp:  
            print "email switched from " + self.renderQ.email_to + " to " + temp
            self.renderQ.email_to = self.buf_email.get()


    def updateQ(self, a=None, b=None, c=None):
        temp = self.buf_ae_lang.get()
        print "lanuage switched from " + self.renderQ.ae_lang + " to " + temp
        self.renderQ.ae_lang = self.buf_ae_lang.get()

    def OnDoubleClick(self, event):
        item = self.tree.selection()[0]
        print("you clicked on", self.tree.item(item,"text"))

        # the command below wont work for some reason.
        #self.handle_editJob()

    def initButtons(self):
        button_chooseAEPath = tk.Button(self.frame_aepath, image=self.image_fileSelect, command=self.handle_setAEPath)
        button_chooseAEPath.grid(row=1,column=2,padx=10)
        button_addJob = tk.Button(self.frame_controls, text="add",image= self.image_plus, command=self.handle_addJob)
        button_addJob.grid(column=2, row=0, sticky=(tk.E, tk.W))
        button_configJob = tk.Button(self.frame_controls, text="edit",image= self.image_config, command=self.handle_editJob)
        button_configJob.grid(column=3, row=0, sticky=(tk.E, tk.W))
        button_removeJob = tk.Button(self.frame_controls, text="remove",image= self.image_minus, command=self.handle_removeJob)
        button_removeJob.grid(column=4, row=0, sticky=(tk.E, tk.W))
        self.button_render = tk.Button(self.frame_controls, text="RENDER ALL",image=self.image_goal, command=self.handle_render)
        self.button_render.grid(column=2, row=1, columnspan=3, pady=10, sticky=(tk.E, tk.W))
        self.button_render.configure(state='disabled')

        tooltip.createToolTip(button_chooseAEPath,"Navigate to your\nAfter Effects\nInstallation Folder.")
        tooltip.createToolTip(button_addJob,"Add a job to the render list.")
        tooltip.createToolTip(button_configJob,"Edit the selected job in the render list.")
        tooltip.createToolTip(button_removeJob,"Remove the selected job from the render list.")
        tooltip.createToolTip(self.button_render,"Start rendering the jobs in the list.")

    def handle_render(self):
        self.progressBar.grid(row=3, column=0, sticky=(tk.W))
        self.progressBar.start()
        self.renderQ.renderAll()
        self.progressBar.stop()
        self.renderQ.ungrid()

    def handle_setAEPath(self, a=None):
        file_path = tkFileDialog.askdirectory(title='Select a Folder')
        # self.entry_aepath.configure(text=file_path)
        self.buf_ae_path.set(file_path )
        self.renderQ.aerender_path = self.buf_ae_path.get()
        print self.entry_aepath.get()

    def handle_editJob(self ):
        # if there is something to edit
        if len(self.renderQ.renderList) > 0:
            #index = int(self.listbox.curselection()[0])
            item_id = self.tree.focus()
            index = self.tree.index(item_id)
            print index

            entry = self.renderQ.renderList[index]
            print "\n________________\nEditing entry for " + str(entry.path_project) + "\nprocs: " + str(entry.instances) + "\ncomp: "+str(entry.comp)+"\nformat: " + str(entry.output_format)+"\npath_output: " + str(entry.path_output)

            self.buf_path_project.set(entry.path_project) 
            self.buf_instances.set(entry.instances) 
            self.buf_format.set(entry.output_format) 
            if entry.comp != "":
                self.buf_comp.set(entry.comp)
                self.buf_startFrame.set(entry.frame_start)
                self.buf_endFrame.set(entry.frame_end)
                self.buf_path_output.set(entry.path_output)
                print "start: "+entry.frame_start+"\nend:"+entry.frame_end
                #jobWindow.enr

            jobWindow = jobSettings(self,True)
            self.master.wait_window(jobWindow)

            proj = self.buf_path_project.get()
            filename = os.path.basename(proj)
            output =  self.buf_path_output.get()
            instances = self.buf_instances.get()
            fileformat = self.buf_format.get()
            comp =  self.buf_comp.get()
            start = self.buf_startFrame.get()
            end = self.buf_endFrame.get()

            self.renderQ.renderList[index].path_project = proj
            self.renderQ.renderList[index].path_output = output
            self.renderQ.renderList[index].instances = instances
            self.renderQ.renderList[index].output_format = fileformat
            self.renderQ.renderList[index].comp = comp
            self.renderQ.renderList[index].frame_start = start
            self.renderQ.renderList[index].frame_end = end

            self.tree.delete(item_id)
            self.tree.insert('', index, text=filename, values=[instances, fileformat, comp, start, end, output])

            self.empty_buffers()

    def handle_addJob(self, a=None):   
        jobWindow = jobSettings(self)
        self.master.wait_window(jobWindow)

        path_project = self.buf_path_project.get()
        formatVal = self.buf_format.get()
        instances = self.buf_instances.get()
        filename = os.path.basename(path_project)

        comp = self.buf_comp.get()
        path_output = self.buf_path_output.get()
        sFrame = self.buf_startFrame.get()
        eFrame = self.buf_endFrame.get()
        if filename.endswith(".aep"):
            print "\n_______________\nAdding Project: " + str(path_project) + " ...\ncomp: "+ str(comp)+"\nprocs:"+ str(instances) +"\nfrom: "+ str(sFrame) +"\nto:"+ str(eFrame) +"\nformat: "+ str(formatVal)+"\npath_output: "+ str(path_output) +"\n"
           # self.listbox.insert(tk.END, filename)

            # ('project', 'instances', 'comp', 'start','end','format','outputPath')
            self.tree.insert('','end',text=filename,values=[instances, formatVal, comp, sFrame, eFrame, path_output])
            self.renderQ.addJob(path_project, instances, formatVal, comp, sFrame,eFrame,path_output)
            print "Adding Project %s ... done." %path_project
            self.button_render.configure(state='active')
        else:
            print "Nothing added. Please check Path/Filename."
            if filename != "":
                tkMessageBox.showinfo("Error","Project path not valid.\nPlease select an After Effects Project file (.aep)")

        self.empty_buffers()
 
    def handle_removeJob(self):
        item_id = self.tree.focus()
        #index = int(trueIndex.split('I')[1])
        index = self.tree.index(item_id)

        print "test: ",index
        print "removing: %i" %index
        self.renderQ.removeJob(index)
        self.tree.delete(item_id)
        print "renderlist:" , str(self.renderQ.renderList)

    def empty_buffers(self):
        print 'emptying buffers.'
        self.buf_path_project.set("")
        self.buf_path_output.set("")
        self.buf_instances.set("")
        self.buf_format.set("")
        self.buf_comp.set("")
        self.buf_startFrame.set("")
        self.buf_endFrame.set("")

if __name__ == '__main__':
    #program = RenderQueueProgram()
    ##program.addJob(r"/home/tux/Documents/RenderQueue/test.aep", 2)
    #program.renderAll()
    renderer = RenderGui()
    #renderer.mainloop()
