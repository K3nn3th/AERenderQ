import Tkinter as Tk


class GUI(Tk.Tk):
    """docstring for GUI"""

    mainFrame = Tk.Frame(None)
    QFrame = Tk.Frame(mainFrame)

    def __init__(self, arg):
            super(GUI, self).__init__()
            self.initFrames()
            self.arg = arg

    def initFrames(self):
            pass

if __name__ == '__main__':
    gui = GUI(None)
    gui.mainloop()
